var currentCard = -1;
var debug = false;
var drawTimer = null;
var flipbackTimer = null;
var drawInterval = 0.2;
var flipbackInterval = 5
var transition = new Transition(Transition.DISSOLVE_TYPE, drawInterval);
var quickTransition = new Transition(Transition.NONE_TYPE, 0);

function load() {
    dashcode.setupParts();
    t = "These cards evolved from separate observations of the principles underlying what we were doing. Sometimes they were recognized in retrospect (intellect catching up with intuition), sometimes they were identified as they were happening, sometimes they were formulated.<br>They can be used as a pack (a set of possibilities being continuously in the mind) or by drawing a single card from the shuffled pack when a dillema occurs in a working situation. In this case the card is trusted even if appropriateness is quite unclear. They are not final, as new ideas will present themselves, and other will become self-evident."
    document.getElementById("cards-text").innerHTML = t;   
}

function remove() {
    clearFlipbackTimer();
}

function hide() {
    clearFlipbackTimer();
    document.getElementById('card-stack').object.setCurrentView('default-view');
}

function show() {
    // No need to restore one shot timers
}

function sync() {
    // This widget has no 'real' preferences
}

function showBack(event) {
    clearFlipbackTimer();
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function showFront(event) {
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";
    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
    document.getElementById('card-stack').object.setCurrentViewWithTransition('default-view',quickTransition);
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

function cardsToFront(event) {
    document.getElementById("tabs").object.setCurrentViewWithTransition("cards-tab", transition);
}

function aboutToFront(event) {
    document.getElementById("tabs").object.setCurrentViewWithTransition("about-tab", transition);
}

function drawACard(event) {
    if (!debug) {
        var cardNo = Math.floor(Math.random() * cards.length);
        while (cardNo == currentCard) {
            cardNo = Math.floor(Math.random() * cards.length);
        }
        currentCard = cardNo;
    } else {
        cardNo = currentCard + 1
        if (cardNo < 0) cardNo = 0;
        if (cardNo >= (cards.length)) cardNo = 0;
        currentCard = cardNo;
    }
    var myStrategy = cards[cardNo];
    var myAttribution = "";
    var bar = myStrategy.indexOf("|");
    if (bar > -1) {
        myAttribution = myStrategy.split("|")[1];
        myStrategy = myStrategy.split("|")[0];
    }
    document.getElementById("strategy").innerHTML = myStrategy;
    document.getElementById("attribution").innerHTML = myAttribution;
    document.getElementById("card-stack").object.setCurrentViewWithTransition("card-draw", transition);
    startFlipbackTimer(flipbackInterval);
}

function drawAnotherCard(event) {
    clearFlipbackTimer();
    document.getElementById("card-stack").object.setCurrentViewWithTransition("blank-view", transition);
    drawNextCard(drawInterval);
}

function drawNextCard(interv) {
    drawTimer = setTimeout(drawNextCardForReal, interv * 1000);
}

function drawNextCardForReal() {
    drawACard();
}

function doneOver(event) {
    document.getElementById("inner_image4").src = "Images/done-over.png";
}

function doneOut(event) {
    document.getElementById("inner_image4").src = "Images/done-out.png";
}

function startFlipbackTimer(interv) {
    if (flipbackTimer != null) {
        clearTimeout(flipbackTimer);
        flipbackTimer = null;
    }
    flipbackTimer = setTimeout(flipCardBack, interv * 1000);
}

function flipCardBack() {
    if (flipbackTimer != null) {
        clearTimeout(flipbackTimer);
        flipbackTimer = null;
    }
    document.getElementById("card-stack").object.setCurrentViewWithTransition("default-view", transition);   
}

function clearFlipbackTimer() {
    if (flipbackTimer != null) {
        clearTimeout(flipbackTimer);
        flipbackTimer = null;
    }
}

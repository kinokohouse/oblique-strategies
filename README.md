# Oblique Strategies #

100+ (actually, 164) worthwhile dilemmas, as compiled by Brian Eno and Peter Schmidt, and now available (again) as a Dashboard widget. Binary versions for older and newer versions of macOS available (see below). MIT License (code only, not the cards of course).


Things to note
--------------

There have been some bugs in Dashboard in later versions of macOS, for example with the positioning of elements that can be one or two pixels off in later OS versions. To work around this, two versions of the widget are available; one for versions up to 10.9 and one for the versions above that. If you find however that the buttons are 'off' on your version of macOS, try the other widget to see if that works, or file a bug report.

Another thing to keep an eye out for on the higher versions (i.e. 10.10 and up) is that the `info` button sometimes does not show on mouse over (it is still there though, it lights up when you click it), and that the `done` button on the back sometimes does not change color on mouse over. To alleviate this, try the following:

* Click inside the widget
* Move your mouse to any other widget on your Dashboard

The above buttons should work as advertised from now on.


Building
--------

Editing and deploying can be done from either version of DashCode, or you can edit the files within the widget bundle with your favourite text/image editors. The supplied version of the Dashcode file is for 10.4 through 10.9, so you might have to make slight adjustments to the button positions if you need to make a version for 10.10 and up.

Version info
------------

Current version is 1.0.
